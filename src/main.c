#include <stdio.h>
#include <errno.h>
#include <string.h>
#include "budmm.h"
#include "hw3.h"

int main(int argc, char const *argv[]) {
    int i;

    bud_mem_init();


    char* ptr1 = (char*)bud_malloc(10); // 64

    //print heap
    /*
    print_header(ptr1);
    print_data();
    printf("\n\n\n\n\n");
    */

    ptr1[0] = 'a';
    ptr1[1] = 'b';
    ptr1[2] = 'c';
    ptr1[3] = 'd';
    ptr1[4] = 'e';
    ptr1[5] = 'f';
    ptr1[6] = '\0';
    printf("ptr1 string: %s\n", ptr1);

    int* ptr2 = bud_malloc(sizeof(int) * 100); // 512
    for(i = 0; i < 100; i++)
        ptr2[i] = i;


    //print heap
    /*
    print_header(ptr2);
    print_data();
    printf("\n\n\n\n\n");
    */


    void* ptr3 = bud_malloc(3000); // 4192
    printf("ptr3: %p\n", ptr3);

    //print heap
    //print_header(ptr3);
    //print_data();

    bud_free(ptr1);
    //bud_free(ptr2);
    //bud_free(ptr3);

    //print heap
    //print_header(ptr3);
    //print_data();
    //exit(0);

    ptr2 = bud_realloc(ptr2, 124); // 128

    ptr1 = bud_malloc(200); // 256
    ptr1 = bud_realloc(ptr1, 100); // 128

    // intentional error (errno = EINVAL)
    ptr3 = bud_malloc(20000);
    printf("errno: %d (%s)\n", errno, strerror(errno));


    bud_mem_fini();

    return EXIT_SUCCESS;
}
