#ifndef HW3_H
#define HW3_H

#include "budmm.h"

/*
* This function returns the pointer to first free block in the circular list
* Returns NULL if not found any.
*/
bud_free_block *cfree_block_finder(bud_free_block* header_arr);

/*
*This is a recursive function which gets free block to array index
*arr_index is value of array index where free block in needed.
*Returns 0 on failure and 1 on success
*/
int get_block(uint32_t arr_index);

/*
*helper to check validity of pointer
*Returns 0 for invalid and 1 for valid
*/
int validate_ptr(void *ptr);

/*
*Help to do coalescing on heap
*It starts from given array index to MAX_ORDER array index
*/
void coalesce(int arr_index);

/*
*Help to get buddy
*if returnValue == NULL then buddy to ptr is not free
*/
bud_free_block *get_buddy (bud_free_block *ptr);

/*
*Function to print data inside the heap.
*Fuctiona calles to bud_listprint and bud_blockparint accordingly.
*/
void print_data();

/*
*Function prinit header of given malloced block's pointer
*/
void print_header(void *ptr);

/*
*This functions removes the first element from circular linked list
*/
void cremove(bud_free_block *block);

/*
*This functions adds the element to circular linked list
*/
void cadd(bud_free_block *block, int arr_index);

#endif