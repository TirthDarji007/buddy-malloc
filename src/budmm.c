/*
 * All functions you make for the assignment must be implemented in this file.
 * Do not submit your assignment with a main function in this file.
 * If you submit with a main function in this file, you will get a zero.
 */
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "debug.h"
#include "budmm.h"
#include "budprint.h"
#include "hw3.h"

/*
 * You should store the heads of your free lists in these variables.
 * Doing so will make it accessible via the extern statement in budmm.h
 * which will allow you to pass the address to sf_snapshot in a different file.
 */
extern bud_free_block free_list_heads[NUM_FREE_LIST];

void *bud_malloc(uint32_t rsize) {

    //Chcek rsize
    if ((rsize == 0) || (rsize > (MAX_BLOCK_SIZE - sizeof(bud_header))) ) {
        errno = EINVAL;
        return NULL;
    }

    uint32_t reqSize = 0;
    int block_status = 0;
    reqSize = rsize + sizeof(bud_header);
    uint32_t alloc_size = MIN_BLOCK_SIZE;
    uint32_t order = ORDER_MIN;
    int arr_index = 0;
    int incr = 0;
    char *payload = NULL;

    while (alloc_size < reqSize) {
        alloc_size *= 2;
        order++;
    }
    arr_index = order - ORDER_MIN;
    debug("allocator size:%d, order to size:%d, requested size:%d, array Idex:%d", alloc_size, ORDER_TO_BLOCK_SIZE(order), rsize, arr_index);

    //get circular list header pointer to the correct order size.
    bud_free_block *ptr_to_arr = &free_list_heads[arr_index];

    // get pointer to free block from circular list.
    bud_free_block *ptr_free_block = cfree_block_finder(ptr_to_arr);;

    // if there is no free block in circular at correct size, then  get correct size pointer
    if ( ptr_free_block == NULL) {
        block_status = get_block( arr_index );
        if (block_status == 0) {
            errno = ENOMEM;
            return NULL;
        }
        ptr_free_block = cfree_block_finder(&free_list_heads[arr_index]);
        if (ptr_free_block == NULL) debug("get_block did not assigned free block properly - malloc function"); //Fatal error. It should never be true during running time.
    }

    // remove ptr from the current circular list
    cremove(ptr_free_block);

    //allocate header properly
    ptr_free_block->header.allocated = 1;
    ptr_free_block->header.order = order;
    ptr_free_block->header.rsize = rsize;
    if (reqSize == alloc_size) ptr_free_block->header.padded = 0;
    else ptr_free_block->header.padded = 1;
    ptr_free_block->header.unused1 = 0;
    ptr_free_block->header.unused2 = 0;

    //Get pointer to payload
    incr = sizeof(bud_header);
    payload = (char*)ptr_free_block + incr;

    return ((void*)payload);
}

void *bud_realloc(void *ptr, uint32_t rsize) {
    //Special case check
    if (ptr == NULL) {
        return bud_malloc(rsize);
    }
    if (rsize == 0) {
        bud_free(ptr);
        return NULL;
    }

    //validate arguments
    if (rsize > (MAX_BLOCK_SIZE - sizeof(bud_header)) ) {
        errno = EINVAL;
        return NULL;
    }
    if (! validate_ptr(ptr)) abort();

    //calculated size is the same as old one: return same pointer
    bud_free_block *old_ptr = (bud_free_block*)(ptr - sizeof(bud_header));
    bud_free_block *buddy_addr = NULL;
    bud_free_block buddy;
    char *temp = NULL;
    uint32_t incr = 0;
    uint32_t old_order = old_ptr->header.order;
    uint32_t payload_size = old_ptr->header.rsize;
    uint32_t new_order = ORDER_MIN;
    uint32_t alloc_size = MIN_BLOCK_SIZE;
    uint32_t reqSize = rsize + sizeof(bud_header);
    while (alloc_size < reqSize) {
        alloc_size *= 2;
        new_order++;
    }

    //If new and old pointer are being assigned to same size block, then return the same pointer given as argument.
    if (old_order == new_order) {
        old_ptr->header.rsize = rsize;
        if (reqSize == alloc_size) old_ptr->header.padded = 0;
        else old_ptr->header.padded = 1;
        return ptr;
    }

    //If the size is bigger
    if (new_order > old_order) {
        void *new_ptr = bud_malloc(rsize); //new_ptr is pointer to new payload
        if (new_ptr == NULL) return NULL; //errorno will be set by bud_malloc
        memcpy(new_ptr, ptr, (ORDER_TO_BLOCK_SIZE(old_order) - sizeof(bud_header))); //copy entire block.
        bud_free(ptr);
        return new_ptr;
    }

    //If the size is smaller
    if (new_order < old_order) {
        while (new_order != old_order) {
            //split into half & adjust order value
            incr = ORDER_TO_BLOCK_SIZE(old_order - 1);
            temp = (char*)old_ptr + incr;
            buddy_addr = (bud_free_block*)temp;
            buddy.header.allocated = 0;
            buddy.header.order = old_order - 1;
            buddy.header.padded = 0;
            buddy.header.rsize = 0;
            buddy.header.unused1 = 0;
            buddy.header.unused2 = 0;
            buddy.next = buddy.prev = NULL;
            old_ptr->header.order = old_order - 1;
            memcpy(buddy_addr, &buddy, sizeof(bud_free_block));
            cadd(buddy_addr, ( (old_order - 1) - ORDER_MIN));
            old_order--;
        }

        //Following  if statement should never be true.
        if ( (old_ptr + sizeof(bud_header)) != ptr ) debug ("bud_realloc for smaller size return different pointer thant provided");

        //change old ptr header to appropriate values.
        old_ptr->header.allocated = 1;
        old_ptr->header.order = new_order;
        old_ptr->header.rsize = rsize;
        if (reqSize == alloc_size) old_ptr->header.padded = 0;
        else old_ptr->header.padded = 1;
        old_ptr->header.unused1 = 0;
        old_ptr->header.unused2 = 0;
        return ptr;
    }

    //fucntion should return before reaching to this line. Otherwise, its and error.
    debug("realloc returning null. It should have been return before returning this value.");
    if ( 0 && payload_size && buddy_addr) debug("clearning unused values.");

    return NULL;
}

void bud_free(void *ptr) {

    if ( ptr == NULL ) return;
    if (! validate_ptr(ptr)) abort();

    void *ptr_head = ptr - sizeof(bud_header);
    bud_free_block* block = (bud_free_block*)ptr_head;
    int order = block->header.order;
    block->header.allocated = 0;
    block->header.rsize = 0;
    block->header.padded = 0;
    block->header.unused1 = 0;
    block->header.unused2 = 0;
    int arr_index = order - ORDER_MIN;

    //Add block to correct list
    cadd(block, arr_index);

    coalesce(arr_index);

    return;
}


/*
* This function returns the pointer to first free block in the circular list
* Returns NULL if not found any.
*/
bud_free_block *cfree_block_finder(bud_free_block* header_arr) {
    bud_free_block* cursor = header_arr->next;
    int allocated;

    //circle and check if any null pointer exist
    while (cursor != header_arr) {
        allocated = cursor->header.allocated;
        if (!allocated ) {
            return cursor;
        }
        cursor = cursor->next;
    }

    return NULL;
}


/*
*This is a recursive function which gets free block to array index
*arr_index is value of array index where free block in needed.
*Returns 0 on failure and 1 on success
*/
int get_block(uint32_t arr_index) {

    if (arr_index >= NUM_FREE_LIST) return 0;

    int response = 0;
    uint32_t order = arr_index + ORDER_MIN;
    uint32_t next_arr_index = arr_index + 1;
    uint32_t incr = 0;
    char *temp = NULL;

    //base condition: call to bud_sbrk to get new memory
    if (order == (ORDER_MAX - 1)) {
        void *ptr = bud_sbrk();
        void *err = (void*) -1;
        if (ptr == err) return 0; // No memory to allocate.

        //create a block
        bud_free_block block;
        block.next = block.prev = NULL;
        block.header.order = ORDER_MAX - 1;
        block.header.allocated = 0;
        block.header.rsize = 0;
        block.header.padded = 0;
        block.header.unused1 = 0;
        block.header.unused2 = 0;
        memcpy(ptr, &block, sizeof(bud_free_block)); //copy header to newly assigned memory

        //add new free block to the list
        bud_free_block *newblock = ptr;
        cadd(newblock, (order - ORDER_MIN));

        return 1;
    }

    if (order >= ORDER_MAX) return 0; //Fatal error. should never be true

    if (next_arr_index >= NUM_FREE_LIST) return 0;
    bud_free_block *ptr_free_block = cfree_block_finder(&free_list_heads[next_arr_index]); // find free block into next array element
    if (ptr_free_block == NULL) {
        response = get_block(next_arr_index);
        if (response == 0) return 0;
        ptr_free_block = cfree_block_finder(&free_list_heads[next_arr_index]);
        if (ptr_free_block == NULL) debug("get_block did not assigned free block properly. Location: get_block function");
    }

    //split the block and add them to arr_index

    //remove free block from the next_arr_index
    cremove(ptr_free_block);

    //split into half & adjust order value
    incr = ORDER_TO_BLOCK_SIZE(order);
    temp = (char*)ptr_free_block + incr;
    bud_free_block *buddy_addr = (bud_free_block*)temp;
    bud_free_block buddy;
    buddy.next = buddy.prev = NULL;
    buddy.header.allocated = 0;
    buddy.header.order = order;
    buddy.header.padded = 0;
    buddy.header.rsize = 0;
    buddy.header.unused1 = 0;
    buddy.header.unused2 = 0;
    ptr_free_block->header.order = order;
    memcpy(buddy_addr, &buddy, sizeof(bud_free_block));

    //Add to the both block to requested array. This order is important since first block to sentinel will allocated first.
    cadd(buddy_addr, arr_index);
    cadd(ptr_free_block, arr_index);

    return 1;
}

/*
*helper to check validity of pointer
*Returns 0 for invalid and 1 for valid
*/
int validate_ptr(void *ptr) {

    void *ptr_head = ptr - sizeof(bud_header);
    uint32_t ptr_addr = ptr_head - bud_heap_start();
    if (! ((ptr >= bud_heap_start()) && (ptr < bud_heap_end()))) return 0;
    if (! ((ptr_head >= bud_heap_start()) && (ptr_head < bud_heap_end()))) return 0;
    if (! ((ptr_addr % MIN_BLOCK_SIZE) == 0)) return 0;
    if ( (uintptr_t)ptr & 0x07) return 0; // if pointer is not a multimple of 8

    bud_free_block* block = (bud_free_block*)ptr_head;
    uint32_t order = block->header.order;
    uint32_t allocated = block->header.allocated;
    uint32_t padded = block->header.padded;
    uint32_t rsize = block->header.rsize;
    if (! ( (order >= ORDER_MIN) && (order < ORDER_MAX) ) ) return 0;
    if ( allocated == 0 ) return 0;

    if ( (rsize + sizeof(bud_header)) == ORDER_TO_BLOCK_SIZE(order) ) {
        if (padded == 1 ) return 0;
    } else {
        if (padded == 0 ) return 0;
    }

    //find the correct order from rsize and compare it to stored order
    uint32_t alloc_size = MIN_BLOCK_SIZE;
    uint32_t orderc = ORDER_MIN;
    while (alloc_size < (rsize + sizeof(bud_header))) {
        alloc_size *= 2;
        orderc++;
    }
    if (order != orderc ) return 0;

    return 1;
}

/*
*Help to do coalescing on heap
*It starts from given array index to MAX_ORDER array index
*/
void coalesce(int arr_index) {
    //Base condition with arr_index checking
    if ( (arr_index < 0) || (arr_index >= (NUM_FREE_LIST - 1)) ) return;
    int isCoalesce = 0;
    bud_free_block *header = &free_list_heads[arr_index];
    bud_free_block *cursor = header->next;
    bud_free_block *newblock = NULL;
    bud_free_block *buddy = NULL;
    while (cursor != header) {
        buddy = get_buddy(cursor);
        if ( buddy != NULL) {
            newblock = ( cursor < buddy ) ? cursor : buddy; //whichever block has smaller address, that block will be header for new block
            newblock->header.order = (cursor->header.order) + 1;
            cremove(cursor);
            cremove(buddy);
            cadd(newblock, arr_index + 1);
            isCoalesce = 1;
            cursor = header->next;
        } else {
            cursor = cursor->next;
        }
    }
    if (isCoalesce) coalesce(arr_index + 1);
}

/*
*Help to get buddy
*if returnValue == NULL then buddy to ptr is not free
*/
bud_free_block *get_buddy (bud_free_block *ptr) {
    bud_free_block *buddy = NULL;
    uint32_t order = ptr->header.order;
    uint32_t size = ORDER_TO_BLOCK_SIZE(order);
    buddy = (bud_free_block*) ( (uintptr_t)ptr ^ (uintptr_t)size );
    if ( buddy->header.allocated ) return NULL;
    if ( buddy->header.order != order) return NULL;
    return buddy;
}

/*
*Function to print data inside the heap.
*Fuctiona calles to bud_listprint and bud_blockparint accordingly.
*/
void print_data() {
    printf("Printing Data\n");
    for (int i = 0; i < NUM_FREE_LIST; i++) {
        bud_listprint(i);
        bud_free_block *header = &free_list_heads[i];
        bud_free_block *cursor = header->next;
        while (header != cursor) {
            bud_blockprint(&cursor->header);
            cursor = cursor->next;
        }
    }

}

/*
*Function prinit header of given malloced block's pointer
*/
void print_header(void *ptr) {
    printf("Printing header\n");
    bud_blockprint((ptr - sizeof(bud_header)));
}

/*
*This functions removes the element from circular linked list
*/
void cremove(bud_free_block *block) {
    block->prev->next = block->next;
    block->next->prev = block->prev;
    block->next = block->prev = NULL;
}

/*
*This functions adds the element from circular linked list
*/
void cadd(bud_free_block *block, int arr_index) {
    block->next = free_list_heads[arr_index].next;
    block->prev = &free_list_heads[arr_index];
    block->next->prev = block;
    free_list_heads[arr_index].next = block;
}